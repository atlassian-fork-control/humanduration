Human Duration library
======================

This library allows to parse Java 8's `Duration` from human readable strings like `2 minutes 39 seconds`, `2m 39s`, etc.

## Usage
 
```java
HumanDuration.stringToDuration("1 hour 2 minutes 3 seconds");
```

## Get it

#### Maven

```xml
<dependency>
    <groupId>com.atlassian.humanduration</groupId>
    <artifactId>humanduration</artifactId>
    <version>0.1</version>
</dependency>
```

#### Gradle

```groovy
compile "com.atlassian.humanduration:humanduration:0.1"
```

## Spring Boot

To enable Human Duration for reading properties in your Spring Boot app, just use the following dependency instead:

#### Maven

```xml
<dependency>
    <groupId>com.atlassian.humanduration</groupId>
    <artifactId>humanduration-spring-boot-starter</artifactId>
    <version>0.1</version>
</dependency>
```

#### Gradle

```groovy
compile "com.atlassian.humanduration:humanduration-spring-boot-starter:0.1"
```

If you use [auto configuration](http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-auto-configuration.html),
you're all set. If you don't, import Human Duration configuration class `com.atlassian.humanduration.spring.HumanDurationAutoConfiguration`. 

## Important notes

Human readable duration is a sequence of pairs of positive numbers followed by their time units.
They _might be_ delimited with whitespace symbols, but the delimiters are not required.

Durations of day and week are estimated and don't track daylight saving time or leap seconds.

Following time units and their textual representations are supported:
 
| Time unit   | Textual representation                   | Notes    |
| ----------- | ---------------------------------------- | -------- |
| nanosecond  | ns ; nanos ; nanosecond ; nanoseconds    |          |
| microsecond | micros ; microsecond ; microseconds      |          |
| millisecond | ms ; millis ; millisecond ; milliseconds |          |
| second      | s ; second ; seconds                     |          |
| minute      | m ; minute ; minutes                     |          |
| hour        | h ; hour ; hours                         |          |
| day         | d ; day ; days                           | 24 hours |
| week        | w ; week ; weeks                         | 7 days   |

## Contributing

Pull requests, issues and comments welcome ☺. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style (always use braces, 4 space indent)
* Separate unrelated changes into multiple pull requests

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
